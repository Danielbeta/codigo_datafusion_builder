#include <Wire.h>
#include <SparkFun_APDS9960.h>
#include <VL53L0X.h>
#include <MB1242.h>
MB1242   sonar;
VL53L0X  laser;
/* Car parts */
class Sonarsensor{
  
  public: 
    void sonarinit(){
    sonar.begin();
    }
    int sonarget(){
    int data= sonar.getDistance();
    return (data);
    }
    };

class modulo{
    public:
      Sonarsensor* ultrasonic;
       
    
  };
class Builder
{
    public:
        virtual Sonarsensor* getultrasonic() = 0;
  
};
class Director
{
    Builder* builder;

    public:
        void setBuilder(Builder* newBuilder)
        {
            builder = newBuilder;
        }

        modulo* getmodulo()
        {
            modulo* Modulo = new modulo();

            Modulo->ultrasonic = builder->getultrasonic();
            return Modulo;
        }
};

class modulodelantero : public Builder
{
    public:
        Sonarsensor* getultrasonic()
        {
            Sonarsensor* ultrasonic = new Sonarsensor();
            int datofinal=ultrasonic->sonarget();
            return datofinal;
        }
};

void setup() {
  // put your setup code here, to run once:
 Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
 modulo* Modulo;
 Director director;
 modulodelantero Modulodelantero;
 director.setBuilder(&Modulodelantero);
 Modulo = director.getmodulo();
 //Serial.println(Modulodelantero.getultrasonic());
}
